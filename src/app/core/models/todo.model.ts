import { ITodoType } from "../../shared/components/todo-card/todo-card.component";

export interface ITodo {
    id?: string;
    name: string;
    desc: string;
    status: ITodoType;
    created?: string;
    created_at?: string;
    updated_at?: string;
  }