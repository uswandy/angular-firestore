import { inject, Injectable } from '@angular/core';
import { doc, docData, DocumentReference, Firestore, getDoc, setDoc, updateDoc, collection, addDoc, deleteDoc, collectionData, query, where, getDocs } from  "@angular/fire/firestore";
import { ITodo } from '../models/todo.model';
import { from, map, Observable } from 'rxjs';

// https://firebase.google.com/codelabs/firebase-web-io23#6
// https://firebase.google.com/docs/firestore/query-data/get-data

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {
  firestore: Firestore = inject(Firestore);
  constructor() { }

  getDocData(path: string) {
    return  docData(doc(this.firestore, path), {idField:  'id'}) as  Observable<ITodo | any>
  }

  filterCollectionData(path: string, status: string) {
    let queryCollection;
    if(status == ''){
      queryCollection = query(collection(this.firestore, path));
    } else {
      queryCollection = query(collection(this.firestore, path), where("status", "==", status));
    }
    
    // from: convert the Promise into an Observable
    return from(getDocs(queryCollection)).pipe(
      map(querySnapshot => querySnapshot.docs.map(doc => {
        const todo: ITodo = doc.data() as ITodo
        todo.id = doc.id
        return todo;
      }))
    )
  }

  async  addDocData(path: string, data: ITodo | any) {
    const  ref = await addDoc(collection(this.firestore, path), data)
    setDoc(ref, {... data, id:  ref.id})
  }

  async  updateData(path: string, data: Partial<ITodo | any>) {
    await  updateDoc(doc(this.firestore, path), data)
  }

  async  deleteData(path: string) {
      const  ref = doc(this.firestore, path);
      await  deleteDoc(ref)
  }

}
