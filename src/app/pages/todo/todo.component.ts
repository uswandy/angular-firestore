import { Component, inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, ReactiveFormsModule } from '@angular/forms';
import { ITodo } from '../../core/models/todo.model';
import { ITodoStatus, TodoCardComponent } from '../../shared/components/todo-card/todo-card.component';
import { Observable } from 'rxjs';
import { AsyncPipe } from '@angular/common';
import { SidebarModule } from 'primeng/sidebar';
import { FirestoreService } from '../../core/services/firestore.service';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';


@Component({
  selector: 'app-todo',
  standalone: true,
  imports: [TodoCardComponent, AsyncPipe, SidebarModule, ReactiveFormsModule, ToastModule],
  templateUrl: './todo.component.html',
  styleUrl: './todo.component.css',
  providers: [MessageService]
})
export class TodoComponent  implements OnInit {
  todoForm!: FormGroup;
  todoStatus = ITodoStatus;
  todoId: string | null = null;
  filterByStatus = '';
  firestorePath = 'todo';
  //
  sidebarVisible: boolean = false;
  todos$: Observable<ITodo[]> | undefined;
  firestoreService: FirestoreService = inject(FirestoreService);

  constructor(private fb: FormBuilder, private messageService: MessageService) {
    this.todoForm = this.fb.group({
      name: new FormControl('', [Validators.required]),
      desc: new FormControl('', [Validators.required]),
      status: new FormControl('OPEN', [Validators.required]),
    });
  }

  ngOnInit(): void {
    this.getAllTodos();
  }

  getAllTodos() {
    this.todos$ = this.firestoreService.filterCollectionData(this.firestorePath, '');
  }

  onCloseSlidePanel() {
    this.sidebarVisible = false;
  }

  onFilterByStatus(status: string) {
    this.filterByStatus = status;
    this.todos$ = this.firestoreService.filterCollectionData(this.firestorePath, status);
  }

  onLoadTodoForm(item: ITodo) {
    this.todoId = item.id!!;
    this.todoForm.patchValue({
      name: item.name,
      desc: item.desc,
      status: item.status,
    });
    this.sidebarVisible = true;
  }

  onSubmit() {
    if (this.todoForm.valid) {
      if (this.todoId) {
        
        this.firestoreService.updateData(`${this.firestorePath}/${this.todoId}`, this.todoForm.value).then(val => {
          this.sidebarVisible = false;
          this.messageService.add({severity:'success', summary: 'Updated', detail: 'Updated'})
          this.todoForm.reset();
          this.todoId = null;
          this.onFilterByStatus(this.filterByStatus);
        })

      } else {
        
        this.firestoreService.addDocData(this.firestorePath, this.todoForm.value).then(val => {
          this.sidebarVisible = false;
          this.messageService.add({severity:'success', summary: 'Success', detail: 'Success'})
          this.todoForm.reset();
          this.onFilterByStatus(this.filterByStatus);
        });
        
      }
      
    }
  }
}
